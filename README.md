# README @ts-wc

<!-- toc -->

- [Introduction](#introduction)
- [Lit-element](#lit-element)
  - [Template](#template)
  - [Property](#property)
  - [Events](#events)
  - [Lifecycle Hooks](#lifecycle-hooks)
  - [Styles](#styles)
- [Usecases which should be solvable](#usecases-which-should-be-solvable)
  - [Angular integration](#angular-integration)
  - [Content projection](#content-projection)
    - [Nested webcomponents](#nested-webcomponents)
    - [Children access](#children-access)
    - [Manipulation of the content projection](#manipulation-of-the-content-projection)
    - [Public API access in showcase](#public-api-access-in-showcase)
  - [Styling](#styling)
    - [CSS custom properties](#css-custom-properties)
    - [styling specific parts](#styling-specific-parts)
  - [Icon Integration](#icon-integration)
  - [Font integration](#font-integration)

<!-- tocstop -->

## Introduction

This is a collection of some web components in an integrated Angular showcase. These collection are not for the public
usage and show only the possiblities of web components with a ShadowDOM and how some issues can be solved.

## Lit-element

### Template

### Property

### Events

### Lifecycle Hooks

### Styles

## Usecases which should be solvable

### Angular integration

### Content projection

#### Nested webcomponents

#### Children access

#### Manipulation of the content projection

-> slotchange

#### Public API access in showcase

### Styling

#### CSS custom properties

#### styling specific parts

### Icon Integration

### Font integration
