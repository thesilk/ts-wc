import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';

import '@ts/wc/lib/stepper.js';
import '@ts/wc/lib/step.js';
import '@ts/wc/lib/icon.js';
import '@ts/wc/lib/button.js';
import '@ts/wc/lib/button-group.js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements AfterViewInit {
  title = 'demo';
  toggleState = true;

  @ViewChild('stepper') stepper: ElementRef = {} as ElementRef;

  public ngAfterViewInit(): void {
    console.log(this.stepper);
  }

  public toggle(_event: Event): void {
    this.toggleState = !this.toggleState;
  }

  public cancelStepper(_event: Event): void {
    this.stepper.nativeElement.cancel();
  }

  public finishStepper(_event: Event): void {
    console.log('finish stepper process');
  }
}
