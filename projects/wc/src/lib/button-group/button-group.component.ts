import { css, CSSResult, customElement, html, LitElement, TemplateResult, unsafeCSS } from 'lit-element';

// @ts-ignore
import style from './button-group.component.scss';
// const style = ':host {background-color: cyan;display: block;}';

@customElement('ts-button-group')
export class ButtonGroupComponent extends LitElement {
  public static get styles(): CSSResult {
    return unsafeCSS(style);
  }

  public render(): TemplateResult {
    return html`
      <h2>Button-Group</h2>
      <ts-button text="Cancel"></ts-button>
      <ts-button text="Ok"></ts-button>
    `;
  }
}
