import { LitElement, html, customElement, css } from 'lit-element';

@customElement('cs-alert')
export class AlertComponent extends LitElement {
  static get styles() {
    return css`
      .alert {
        border: 1px solid red;
        padding: 1rem;
        padding-bottom: 0;
      }

      .alert-error {
        display: inline-flex;
        color: red;
        align-items: center;

        span {
          padding: 1rem;
        }

        p {
          margin: 1rem;
        }
      }
    `;
  }
  // constructor() {
  // super();
  // }

  render() {
    return html`
      <div class="alert">
        <div class="alert-error">
          <span>!</span>
          <p>This is an alert message</p>
        </div>
        <div class="alert-button">
          <button>Show something!</button>
        </div>
      </div>
    `;
  }
}
