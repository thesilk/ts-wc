// import './button.component.css';
// @ts-ignore
import btnStyle from './button.component.scss';

class ButtonComponent extends HTMLElement {
  private _text: string = '';
  private _style: HTMLStyleElement;

  constructor() {
    super();

    this._style = document.createElement('style');
    this._style.textContent = btnStyle;
  }

  public connectedCallback() {
    const textNode = document.createTextNode(this._text);
    const button: HTMLButtonElement = document.createElement('button');
    button.classList.add('btn');
    button.appendChild(textNode);
    this.append(this._style, button);
  }

  static get observedAttributes() {
    return ['text'];
  }

  public attributeChangedCallback(name: string, oldValue: any, newValue: any): void {
    if ((name = 'text')) {
      this._text = newValue;
    }
    // called when one of attributes listed above is modified
    //
  }
}

customElements.define('ts-button', ButtonComponent);
