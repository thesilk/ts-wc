import { LitElement, html, customElement, css, property } from 'lit-element';

@customElement('ts-icon')
export class IconComponent extends LitElement {
  @property() public icon: string = '';

  private _iconContent: string = '';

  static get styles() {
    return css`
      .fa-icon::before {
        font-family: var(--ts-icon-font-family, 'Font Awesome 5 Free');
        background-color: var(--ts-icon-background-color, transparent);
        color: var(--ts-icon-color, black);
        font-size: var(--ts-icon-font-size, 24px);
        content: var(--ts-icon-content);
        font-style: normal;
        line-height: 1;
      }
    `;
  }

  private fetchIconContent(): void {
    const data = require('./codepoints.json');
    if (this.icon) {
      this._iconContent = data[this.icon];
    }
  }

  public attributeChangedCallback(name: string, _oldValue: any, newValue: any): void {
    if (name === 'icon') {
      this.icon = newValue;
      this.fetchIconContent();
    }
  }

  public disconnectedCallback(): void {}

  render() {
    return html`<i part="icon" class="fa-icon" style="--ts-icon-content: ${'"' + this._iconContent + '"'}"></i>`;
  }
}
