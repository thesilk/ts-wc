import { LitElement, html, customElement, queryAssignedNodes, TemplateResult } from 'lit-element';
import { StepperService } from './stepper.service';
import { btnBar, btnStep, host, stepContent, stepNavigation, tsIcon } from './stepper.styles';

@customElement('ts-stepper')
export class StepperComponent extends LitElement {
  @queryAssignedNodes('step', true) private _stepItems: any;

  public get currentStep(): number {
    return this._currentStep;
  }

  public set currentStep(val: number) {
    this._currentStep = val;
    this.requestUpdate();
  }

  private _currentStep = 0;

  public get stepItems(): NodeListOf<HTMLElement> {
    return this._stepItems;
  }

  static get styles() {
    return [host, stepNavigation, stepContent, tsIcon, btnStep, btnBar];
  }

  constructor(private _service: StepperService = new StepperService()) {
    super();
  }

  public firstUpdated(): void {
    this.showCurrentStep({} as Event);
    console.log(this._service.getStepStructure(this.stepItems));
    this.requestUpdate();
  }

  public disconnectedCallback(): void {}

  public showCurrentStep(_event: Event) {
    const index = parseInt((_event?.target as HTMLElement)?.getAttribute('data-index') || '0');
    this.currentStep = index;
    this.setCurrentStepActive();
    this._service.activateStep(this.stepItems, index);
  }

  private setCurrentStepActive(): void {
    this.shadowRoot?.querySelectorAll('.btn-step').forEach((btn) => btn.classList.remove('btn-step-active'));
    this.shadowRoot?.querySelector(`button[data-index='${this.currentStep}']`)?.classList.add('btn-step-active');
  }

  public createStepButtons(): TemplateResult | TemplateResult[] {
    if (this.stepItems) {
      return Array.from(this.stepItems).map(
        (_val: HTMLElement, idx: number) =>
          html`<button class="btn-step" data-index=${idx} @click="${this.showCurrentStep}">
            ${this.createButtonNode(idx)}
          </button>`
      );
    }
    return html``;
  }

  private createButtonNode(idx: number): TemplateResult {
    if (idx === this.currentStep) {
      return html`<ts-icon icon="fa-pen"></ts-icon>`;
    }
    return html`${idx + 1}`;
  }

  public createNextBtnLabel(): TemplateResult {
    if (this.stepItems) {
      if (this.currentStep === this.stepItems.length - 1) {
        return html`Send`;
      }
    }
    return html`Next`;
  }

  public cancel(): void {
    this.currentStep = 0;
    this.gotoStep(0);
  }

  public nextStep(): void {
    if (this.stepItems) {
      if (this.currentStep + 1 < this.stepItems.length) {
        this.currentStep++;
        this.gotoStep(this.currentStep);
      } else {
        this.dispatchEvent(this._service.createFinishEvent());
        this.cancel();
      }
    }
  }

  public previousStep(): void {
    this.currentStep--;
    this.gotoStep(this.currentStep);
  }

  private gotoStep(index: number): void {
    this.setCurrentStepActive();
    this._service.activateStep(this.stepItems, index);
  }

  public handleSlotchange(event: Event): void {
    this.showCurrentStep(event);
    this.requestUpdate();
  }

  public render(): TemplateResult {
    return html`
      <div class="stepper">
        <div class="step-navigation">${this.createStepButtons()}</div>
        <div class="step-content">
          <slot name="step" @slotchange=${this.handleSlotchange}></slot>
        </div>
        <div class="button-bar">
          ${this.currentStep > 0
            ? html`
                <button @click="${this.cancel}" class="btn">Cancel</button>
                <button @click="${this.previousStep}" class="btn">Back</button>
              `
            : html``}
          <button part="nextButton" @click="${this.nextStep}" class="btn btn-primary">
            ${this.createNextBtnLabel()}
          </button>
        </div>
      </div>
    `;
  }
}
