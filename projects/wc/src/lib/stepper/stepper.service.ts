interface IStepStructure {
  index: number;
  subSteps: number[];
}

export class StepperService {
  public activateStep(stepItems: NodeListOf<HTMLElement>, index: number): void {
    stepItems.forEach((val: HTMLElement, key: number) => {
      key !== index ? val.setAttribute('hidden', 'true') : val.removeAttribute('hidden');
    });
  }

  public createFinishEvent(): CustomEvent {
    return new CustomEvent('finish-stepper', {
      detail: { message: 'finish stepper' },
      bubbles: true,
      composed: true,
    });
  }

  public getStepStructure(stepItems: NodeListOf<HTMLElement>): IStepStructure[] {
    let children: number[] = [];
    const stepStructure: IStepStructure[] = [];
    stepItems.forEach((val: HTMLElement, index: number) => {
      children = [];

      Array.from(val.children).forEach((val: Element, idx: number) => {
        if (val.nodeName === 'TS-STEP') {
          children.push(idx);
        }
      });

      stepStructure.push({ index, subSteps: children });
    });
    return stepStructure;
  }
}
