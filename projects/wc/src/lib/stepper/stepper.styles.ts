import { css } from 'lit-element';

export const host = css`
  :host {
    display: block;
    margin: 1rem 0rem;
  }
`;

export const stepNavigation = css`
  .step-navigation {
    display: flex;
    justify-content: space-between;
    background: linear-gradient(
      180deg,
      rgba(0, 0, 0, 0) calc(50% - 1px),
      rgba(192, 192, 192, 1) calc(50%),
      rgba(0, 0, 0, 0) calc(50% + 1px)
    );
  }
`;

export const stepContent = css`
  .step-content {
    margin: 1rem 0rem;
  }
`;

export const btnStep = css`
  .btn-step {
    width: 50px;
    height: 50px;
    text-align: center;
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.33px;
    border-radius: 25px;
    color: #fff;
    background-color: #39b3d7;
    border: 1px solid #269abc;
  }

  .btn-step:hover,
  .btn-step:focus,
  .btn-step-active {
    background-color: grey;
    border: 1px solid #fff;
  }
`;

export const tsIcon = css`
  ts-icon {
    --ts-icon-font-size: 18px;
    --ts-icon-color: white;
  }
`;

export const btnBar = css`
  .btn {
    height: 30px;
    width: 100px;
    text-align: center;
    font-size: 16px;
    border: 1px solid black;
    border-radius: 7px;
    background-color: white;
    margin-left: 0.25rem;
  }

  .btn-primary {
    color: #fff;
    background-color: #39b3d7;
    border: 1px solid #269abc;
  }

  .btn:hover,
  .btn:focus {
    background-color: lightgrey;
  }

  .btn-primary:hover,
  .btn-primary:focus {
    background-color: #269abc;
    border: 1px solid #fff;
  }

  .button-bar {
    display: flex;
    justify-content: right;
    padding-top: 1rem;
    border-top: 1px solid lightgrey;
  }
`;
