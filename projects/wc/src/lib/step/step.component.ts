import { LitElement, html, customElement, property, TemplateResult } from 'lit-element';

@customElement('ts-step')
export class StepComponent extends LitElement {
  @property() public hidden: boolean = false;

  public attributeChangedCallback(name: string, _oldValue: any, newValue: any): void {
    if (name === 'hidden') {
      this.hidden = !!newValue;
    }
  }

  private createNextBtnLabel(): TemplateResult {
    return this.hidden !== true ? html`<slot></slot>` : html``;
  }

  render() {
    return this.createNextBtnLabel();
  }
}
